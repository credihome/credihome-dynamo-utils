const { _tag, _fillGap } = require('../_internal');

/**
 * Retorna uma função com os argumentos e suas respectivas tags.
 * (**func(#arg, :arg)**)
 * @param {string} func - Função de query do DynamoDB
 * @param {string[]} args - Argumentos da função, de acordo com atributos disponíveis
 * nos parametros.
 * @param {string[]} types - Tag dos atributos, respeitando a ordem das arrays
 */
module.exports.expressionFunction = (func, args, types) => {
  args = _fillGap(args, types);

  const keys = Object.keys(args);
  args = keys.map(key => _tag(args[key], types[key]))

  return `${func}(${args.join(', ')})`
}
