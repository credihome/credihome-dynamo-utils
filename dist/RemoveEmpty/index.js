/**
 * Remove todas as propriedades vazias em um objeto. Condições para apagar:
 * **{}**, **[]**, **""**, **null** ou **undefined**
 * @param {any} obj - Objeto a ser limpado
 */
const removeEmpty = (obj) => {
  if (obj !== null && obj !== undefined && typeof obj !== 'number') {
    const keys = Object.keys(obj);
    keys.map(key => {
      if (typeof obj[key] !== 'string' && typeof obj[key] !== 'boolean') {
        obj[key] = removeEmpty(obj[key]);
      }

      try { obj[key] = obj[key].filter(item => item != null) } catch (e) {}

      if (obj[key] === undefined || obj[key] === '') delete obj[key];
    });

    if (Object.keys(obj).length == 0) return undefined;
  }
  return (obj === null) ? undefined : obj;
}

module.exports = {
  removeEmpty
}
