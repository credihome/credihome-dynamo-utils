const RE = require('./index').removeEmpty

describe('test RemoveEmpty 1', () => {
  it('teste simples de objetos', () => {
    expect.assertions(10)

    expect(1).toBe(1)

    expect(RE({
      "duration": 360,
      "includeRegistryFees": true
    })).toMatchObject({
      "duration": 360,
      "includeRegistryFees": true
    })

    expect(RE({ a: null, b: 'asd', c: 123 }))
      .toMatchObject({ b: 'asd', c: 123 })
    expect(RE({ a: null, b: 'asd', c: 123 }).a)
      .toBeUndefined()

    expect(RE({ a: '', b: 'asd', c: 123 }))
      .toMatchObject({ b: 'asd', c: 123 })
    expect(RE({ a: '', b: 'asd', c: 123 }).a)
      .toBeUndefined()

    expect(RE({ a: undefined, b: 'asd', c: 123 }))
      .toMatchObject({ b: 'asd', c: 123 })
    expect(RE({ a: undefined, b: 'asd', c: 123 }).a)
      .toBeUndefined()

    expect(RE({ a: {}, b: 'asd', c: 123 }))
      .toMatchObject({ b: 'asd', c: 123 })
    expect(RE({ a: {}, b: 'asd', c: 123 }).a)
      .toBeUndefined()
  })
})
