const { _single, _operator } = require('../_internal');

/**
 * Junta expressões através de um operador. (**exp and (exp) <> (exp)**)
 * @param {string[]} expressions - Expressões ou valores qu podem ser comparados por operador
 * @param {string[]} operator - Operadores que juntaram expressões ou valores comparaveis.
 */
module.exports.expressionJoin = (expressions, operator) => {
  let combined = expressions.shift();

  const keys = Object.keys(expressions);
  keys.map(key => {
    combined += _operator(_single(operator, key)) + `(${expressions[key]})`;
  })
  return combined;
}
