const { expression } = require('../expression');
const { expressionJoin } = require('../expressionJoin');
const { _single, _fillGap } = require('../_internal');

/**
 * Retorna uma única expressão derivada das diversas expressões
 * individuais criadas. Para cada atributo, é gerado uma expressão individual
 * (**attr = attr**), que então é juntada a outra por um operador.
 * (**attr = attr and (attr <> attr)**)
 * @param {string[]} attr - Lista de atributos que terão uma expressão
 * @param {string[]} operator - Lista de operadores para cada atributo
 * @param {string[]} joinOp - Lista de operadores para juntar as expressões
 */
module.exports.expressions = (attr, operator, joinOp) => {
  const expressions = [];
  const keys = Object.keys(attr);
  operator = _fillGap(operator, keys);
  keys.map(key => {
    expressions.push(expression(attr[key], _single(operator, key)))
  })
  return expressionJoin(expressions, joinOp);
}
