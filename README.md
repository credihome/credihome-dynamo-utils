# credihome-dynamo-utils

## _internal *./dist/internal*
Funções que são usadas pelas outras funções do módulo, evitando quebras de sintaxes na expressão final do DynamoDB.

---
### _single (obj, key)

Função que retorna o valor de uma propriedade ou índice no objeto/array, mas que automaticamente retorna o próprio valor original caso não seja uma das opções.

**Parametros**

#### obj (any)

> Variável que possui ou índice ou propriedade, se não, o retorno da função.


#### key (string || number)

> Índice ou propriedade que será procurada em **obj** se o mesmo for um objeto ou array.


|           obj           | key | Resultado |
|:-----------------------:|:---:|:---------:|
| { a: 16, b: 32, c: 64 } | 'b' |     32    |
|       [16, 32, 64]      |  0  |     16    |
|          'yudi'         | 'c' |   'yudi'  |

---
### _operator (op)

Função que retorna operadores lógicos usados pelo DynamoDB, com o valor padrão sendo **and**.

**Parametro**

#### op (string)

> String que representa o operador lógico, ignorando capitalização das letras.


|     op    | Resultado |  Traduz para |      Observação      |
|:---------:|:---------:|:------------:|:--------------------:|
|    'EQ'   |   ' = '   |    Igual a   |   Equivalente a ==   |
|   'NOT'   |   ' <> '  | Diferente de |   Equivalente a !=   |
|    'LE'   |   ' <= '  |  Menor igual |                      |
|    'LT'   |   ' < '   |   Menor que  |                      |
|    'GE'   |   ' >= '  |  Maior igual |                      |
|    'GT'   |   ' > '   |   Maior que  |                      |
|   'AND'   |  ' and '  |       e      |   Equivalente a &&   |
| undefined |  ' and '  |       e      | Valores não listados |

Querys no DynamoDB utilizam esses operadores em propriedades como **KeyConditionExpression** ou **FilterExpression**.

```
partnerId <> '8192' and (managerId = '23')
```

No caso dos parametros de execução do DynamoDB, será comum montar expressões como essa:

```
#partnerId <> :partnerId and (#managerId = :managerId)
```

---
### _tag (item, type)

Retorna uma referencia a um atributo presente nos parametros do DynamoDB, **ExpressionAttributeValues** ou **ExpressionAttributeNames**, com respectivamente: **:exemplo** ou **#exemplo**.

**Parametros**

#### item (string)

> Nome do atributo que será referenciado na expressão.


#### type (string)

> tag usada no nome do atributo. Retorna o valor imputado se não for **name** ou **value**; considere a capacidade do DynamoDB de interpretar expressões não documentadas no SDK da [Amazon](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Query.html).


|   type  |     item    |   Resultado  |           Referencia          |
|:-------:|:-----------:|:------------:|:-----------------------------:|
|  'name' | 'partnerId' | '#partnerId' |  Nome do atributo 'partnerId' |
| 'value' | 'partnerId' | ':partnerId' | Valor do atributo 'partnerId' |
|   '#'   | 'managerId' | '#managerId' |  Nome do atributo 'managerId' |
|   ':'   | 'managerId' | ':managerId' | Valor do atributo 'managerId' |

---
### _fillGap (arrayA, ArrayB)

**Dependências**

- lodash: ^4.17.15

Expande uma array para o tamanho de uma segunda array, preenchendo os novos índices com o último valor da lista. Mesmo se os parametros não forem arrays, serão convertidos para uma array com 1 index (``` arrayA = [arrayA] ```).

```
a = ['partnerId'], b = ['#', ':', 'name']

_fillGap(a, b) = ['partnerId', 'partnerId', 'partnerId']
```
> *Considere o último valor da primeira array quando for preencher a diferença com a segunda array, pois será sempre o valor dos novos índices. Uma melhor utilização dessa função será demonstrada em [expressionFunction](#expressionfunction-func-args-types).*


---
## expression (attr, operator) *./dist/expression*

**Dependências**

- [_operator](#_operator-op) *../_internal*

Retorna uma expressão básica de **name** para **value**.

**Parametros**

#### attr (string)

> Nome do atributo que será usado na expressão.


#### operator (string)

> Operador da expressão, automaticamente tratado em [_operator](#_operator-op).


```
expression('partnerId', 'EQ') = '#partnerId = :partnerId'
```

Aplicação da função é recomendada caso os atributos citados tenham sido expressos corretamente nos parametros do DynamoDB; como no exemplo:

```
{
    ExpressionAttributeValues: {
        ':partnerId': '4002'
    },
    ExpressinonAttributeNames: {
        '#partnerId': 'partnerId'
    }
}
```

---
## expressionFunction (func, args, types) *./dist/expressionFunction*

**Dependências**

- [_fillGap](#_fillgap-arraya-arrayb) *../internal*
- [_tag](#_tag-item-type) *../internal*

Retorna uma função de query com argumentos e suas respectivas tags.

(Se aplica apenas para expressões do Dynamo e não será possível referenciar uma função previamente declarada em um projeto NodeJS)

**Parametros**

#### func (string)

> Nome da função de query do DynamoDB. (Lista de funções documentadas em [Comparison Operator and Function Reference](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Functions))


#### args (string[])

> Lista de argumentos utilizados na função, utilizando [_fillGap](#_fillgap-arraya-arrayb) para repetir o último argumento quando houver mais tags que argumentos imputados.


#### types (string[])

> Lista de tags que serão automaticamente aplicadas com [_tag](#_tag-item-type) e determinarão o número de argumentos na função.


**Melhor uso:**
```
expressionFunction('if_not_exists', ['createdAt'], ['#', ':']) = 'if_not_exists(#createdAt, :createdAt)'
```
> *Cite uma vez o atributo utilizado, e quantas vezes ele será imputado nos parametros, com suas respectivas tags. **if_not_exists** usa uma lógica semelhante em [expression](#expression-attr-operator-distexpression), onde o primeiro parametro é o nome, e o segundo o valor.*

**Incorreto:**
```
expressionFunction('if_not_exists', ['createdAt, updatedAt'], ['#']) = 'if_not_exists(#createdAt)'
```
> *Não será considerado índices extras na primeira array, e possivelmente, DynamoDB irá retornar um erro dependendo da função utilizada.*


No exemplo, cria-se uma função que irá escrever a coluna **createdAt** com o valor armazenado nos parametros em **ExpressionAttributeValues** apenas se não houver valor armazenado anteriormente na tabela.

---
## expressionJoin (expressions, operator) *./dist/expressionJoin*

**Dependências**

- [_single](#_single-obj-key) *../internal*
- [_operator](#_operator-op) *../internal*

Junta expressões através de um operador.

**Parâmetros**

#### expressions (string[])

> Duas ou mais strings que serão combinadas em uma expressão com operador.


#### operator (string[])

> Um ou mais operadores que irão juntar as expressões em sua respectiva ordem. (utiliza as regras de [_operator](#_operator-op) para falta de argumentos)


Condensa a criação de uma condição mais extensa em uma ou duas arrays; como:
```
expressions = ['ffid_lastUpdate <> null', '#partnerId = :partnerId', '#managerId = :managerId']
operators = ['and', 'not']

expressionJoin(expressions, operators) = 'ffid_lastUpdate <> null and (#partnerId = :partnerId) <> (#managerId = :managerId)'
```
> *Com as condições numa array, pode-se juntar todas em uma única expressão, mesmo não definindo operador, pois, por padrão, todas as junções serão **and**.*


---
## expressions (attr, operator, joinOp) *./dist/expressions*

**Dependências**

- [expression](#expression-attr-operator-distexpression) *../expression*
- [expressionJoin](#expressoinjoin-expressions-operator-distexpressionjoin) *../expressionJoin*
- [_single](#_single-obj-key) *../internal*
- [_fillGap](#_fillgap-arraya-arrayb) *../internal*

Gera múltiplas expressões, juntando-as conforme [expressionJoin](#expressoinjoin-expressions-operator-distexpressionjoin). Diferente de expressoinJoin, essa função aceita também os mesmos parametros que [expression](#expression-attr-operator-distexpression), com adição dos operadores que irão juntar cada expressão gerada individualmente.

**Parâmetros**

#### attr (string[])

> Lista de nomes de atributos, assim como expression, previamente definidos nos parametros do DynamoDB, com nome e valor.


#### operator (string[])

> Operadores para respectivamente as expressões dos atributos listados. (O último da lista irá se repetir caso haja menos operadores que atributos)


#### joinOp (string[])

> Segue as mesmas regras de operador em expressionJoin; irá juntar as expressões em sua respectiva ordem.


**Diversas condições simples**

Gere uma mesma comparação para vários atributos que serão testado na tabela
```
attr = [partnerId, managerId, facId]
operator  = 'EQ'

expressions(attr, operator) = '#partnerId = :partnerId and (#managerId = :managerId) and (#facId = :facId)';
```
> *Apenas attr é estritamente requirido como uma array, caso contrário, considere sempre usar a versão singular, **expression** para uma única expressão. **operator** é automaticamente reutilizado em todas as expressões com [_fillGap](#_fillgap-arraya-arrayb), caso não seja imputado **operator**, como sempre, **and** será padrão. (**_fillGap** não se aplica a **joinOp**)*


**Diferencie as primeiras condições**

Defina os primeiros operadores, e use o último para se repetir nas outras expressões.
```
attr = [partnerId, managerId, facId]
operator  = ['EQ', 'NOT']
joinOp = ['NOT']

expressions(attr, operator, joinOp) = '#partnerId = :partnerId <> (#managerId <> :managerId) and (#facId <> :facId)';
```
> *Diferente de **join**, **joinOp** não se repete conforme mais expressões surgem, como surgere o exemplo. Expressões extensas como essa normalmente serão utilizadas em um filtro, onde todas as junções devem permanecer **and**, ainda sim, para qualquer caso, será possível definir esses operadores quando necessário.*


---
## findDynamo (searchValues, columnName) *./dist/findDynamo*

Filtra uma query por uma coluna em multiplos valores escolhidos.

**Parâmetros**

#### searchValues (any[])

> Lista de valores para incluir na coluna selecionada.


#### columnName (string)

> Nome da coluna para filtrar com base nos valores listados.


```
searchValues = [23, 95, 27, 48]
columnName = 'managerId'

findDynamo(searchValues, columnName) = {
    ExpressoinAttributeValues: {
        ':managerId0': 23,
        ':managerId1': 95,
        ':managerId2': 27,
        ':managerId3': 48
    },
    FilterExpression: 'managerId IN (:managerId0, :managerId1, :managerId2, :managerId3)'
}
```
> *Uma expressão **IN** é gerada, onde será mostrado itens cuja coluna **managerId** possui um dos valores listados.*

---
## objToDynamo (payload, partKey, sortKey?, method?) *./dist/objToDynamo*

Traduz um objeto nos parametros do DynamoDB para executar put, update query ou scan e gerar expressões cujo os atributos serão citados.

**Parâmetros**

#### payload (object)

> objeto que terá as propriedades alocadas em **ExpressionAttributeValues** e **ExpressionAttributeNames**. Uma **UpdateExpression** será gerada automaticamente.


#### partKey (string)

> Correspondente ao nome da partition key, ou a chave primária da tabela ou índice. Apontar a coluna da chave primária quando executar updates, para excluir esse mesmo atributo de **UpdateExpression**.


#### sortKey (string) *default = null*

> Correspondente ao nome da sorting key, a chave de ordenação, que serve como chave secundaria da tabela ou índice. Também será excluido de **UpdateExpression** para executar updates; geralmente é apontada quando executa uma query de índice secundário. (quando há uma chave secundária)


#### method (string) *default = 'query'*

> Método que será executado no DynamoDB, nesse caso, utilizado para alocar partKey e sortKey na propriedade Key dos parametros do DynamoDB quando **method** for **update**. Tirando esse caso, não será necessário imputar esse parâmetro.


**Objeto sem apontar chaves.**

```
payload = {
    id: 4002,
    createdAt: '2020-06-15',
    name: 'yudi',
    managerId: 23,
}
```
> *Esse payload serviŕa de exemplo múltiplas vezes.*


```
objToDynamo(payload) = {
    Key: {},
    UpdateExpression: 'SET #id = :id, #createdAt: = :createdAt, #name = :name, #managerId = :managerId',
    ExpressionAttributeValues: {
        ':id': 4002,
        ':createdAt': '2020-06-15',
        ':name': 'yudi',
        ':managerId': 23
    },
     ExpressionAttributeNames: {
        '#id': 'id',
        '#createdAt': 'createdAt',
        '#name': 'name',
        '#managerId': 'managerId'
    },
}
```
> *Esse caso geralmente se aplica para parametros de Put, onde não é necessário preservar **partition key** e **sorting key**.*


**Apontando uma das chaves.**

```
objToDynamo(payload, 'id') = {
    Key: {},
    UpdateExpression: 'SET #createdAt: = :createdAt, #name = :name, #managerId = :managerId',
    ExpressionAttributeValues: {
        ':createdAt': '2020-06-15',
        ':name': 'yudi',
        ':managerId': 23
    },
     ExpressionAttributeNames: {
        '#createdAt': 'createdAt',
        '#name': 'name',
        '#managerId': 'managerId'
    },
}
```
> *O atributo **id** foi removido dos parametros, que será usado em outra propriedade durante a query. A mesma regra se aplica a **sortKey**.*


**Em caso de update.**

```
objToDynamo(payload, 'id', 'createdAt', 'update') = {
    Key: {
        id: 4002,
        createdAt: '2020-06-15'
    },
    UpdateExpression: 'SET #name = :name, #managerId = :managerId',
    ExpressionAttributeValues: {
        ':name': 'yudi',
        ':managerId': 23
    },
     ExpressionAttributeNames: {
        '#name': 'name',
        '#managerId': 'managerId'
    },
}
```
> *Para executar updates, as chaves da tabela devem ser automaticamente colocadas na propriedade **Key**, para ser imediatamente acessada pelo DynamoDB. Lembre-se de utilizar o último parâmetro como **update**.*


---
## removeEmpty (obj) *./dist/removeEmpty*

Remove todas as propriedades vazias em um objeto.

Condição para ser apagada: ```{}```, ```[]```, ```""```, ```null``` ou ```undefined```.

**Parâmetros**

#### obj (any)

> Objeto que será limpado. Qualquer propriedade que estiver em uma das condições citadas será deletada, isso inclui o próprio objeto.

```
obj = {
    name: 'iduy',
    prize: {},
    list: [
        {
            prop1: 'PS',
            prop2: '',
            prop3: 0
        },
        {}
    ],
    party: []
}

removeEmpty(obj) = {
    name: 'iduy',
    list: [
        {
            prop1: 'PS',
            prop3: 0
        }
    ]
}
```

> *Essa função foi projetada apenas para seguir os passos na função [removeEmpty](#removeempty-obj-follow-name-distremoveempty).*


---
## Como escolher sua versão
Caso queira baixar uma versão especifica, verifique a tag e adicione ela no yarn add da seguinte forma:

```
yarn add  <git repository URL>#<version tag>
```
exemplo:
```
yarn add  https://bitbucket.org/credihome/credihome-dynamo-utils.git#0.0.0
```