const { _operator } = require('../_internal');

/**
 * Retorna uma expressão básica de **name** para **value**
 * (**#attr = :attr**)
 * @param {string} attr - Atributo referenciado
 * @param {string} operator - Operador lógico
 */
module.exports.expression = (attr, operator) => {
  return `#${attr}` + _operator(operator) + `:${attr}`;
}
