const base64Encode = (element) => {
  return Buffer.from(JSON.stringify(element)).toString('base64');
}

const base64Decode = (element) => {
  return JSON.parse(Buffer.from(element, 'base64').toString('ascii'))
}

module.exports = {
  base64Encode,
  base64Decode
}
