const _ = require('lodash');

/**
 * Busca retornar o índice ou propriedade do objeto e caso não haja, retorna o valor recebido.
 * @param {any} obj - Valor que possui índice ou propriedade para ser testado
 * @param {string} key - Índice ou nome de propriedade a ser selecionada. (string ou número)
 */
module.exports._single = (obj, key) => {
  return (typeof obj === 'object' && obj !== null) ? obj[key] : obj;
}

/**
 * Retorna os operadores lógicos equivalentes para o DynamoDB
 * @param {string} op - String representando o operador lógico.
 */
module.exports._operator = op => {
  switch (op || op.toUpperCase()) {
    case 'EQ': return ' = ';
    case 'NOT': return ' <> ';
    case 'LE': return ' <= ';
    case 'LT': return ' < ';
    case 'GE': return ' >= ';
    case 'GT': return ' > ';
    default: return ' and ';
  }
}

/**
 * Retorna o nome de uma propriedade descrita em **item** com uma tag
 * especificada em **type**.
 * @param {string} item - Nome da propriedade que estará presente nos parameros
 * do DynamoDB.
 * @param {string} type - Tag que acompanha o nome da propriedade
 * (**value** ou '**:**', **name** ou '**#**').
 */
module.exports._tag = (item, type) => {
  switch (type || type.toLowerCase()) {
    case 'name': return `#${item}`;
    case 'value': return `:${item}`;
    default: return `${type}${item}`;
  }
}

/**
 * Expande a primeira array para o tamanho da segunda, preenchendo
 * os novos índices com o último item da lista.
 * @param {Array} arrayA - Array que será expandida
 * @param {Array} arrayB - Array de referencia
 */
module.exports._fillGap = (arrayA, arrayB) => {
  if (typeof arrayA !== 'object') { arrayA = [arrayA] }
  if (typeof arrayB !== 'object') { arrayB = [arrayB] }
  const lengthA = Object.keys(arrayA).length;
  const lengthB = Object.keys(arrayB).length;

  if (lengthA < lengthB) {
    const gap = Array(lengthB - lengthA).fill(arrayA[lengthA - 1])
    return _.concat(arrayA, gap)
  } else {
    return arrayA;
  }
}
