/**
 * Traduz as propriedades de um objeto em atributos de parametros do DynamoDB.
 * Utilize o método **update** para excluir partKey e sortKey das expressions.
 * @param {object} payload - Objeto que será traduzido para os parametros do DynamoDB
 * @param {string} partKey - Chave primaria para a seleção
 * @param {string} sortKey - Chave de ordenação
 * @param {string} method - Método do request (**query** ou **update**)
 */
const objToDynamo = (payload, partKey, sortKey = null, method = 'query') => {
  let UpdateExpression = 'SET ';
  const ExpressionAttributeValues = {};
  const ExpressionAttributeNames = {};
  const Key = {};

  const keys = Object.keys(payload)
    .filter(e => (e !== partKey && e !== sortKey));
  keys.map(
    key => {
      UpdateExpression += `#${key} = :${key}, `;
      ExpressionAttributeValues[`:${key}`] = payload[key];
      ExpressionAttributeNames[`#${key}`] = key;
    }
  );
  UpdateExpression = UpdateExpression.slice(0, -2);

  if (method === 'update') {
    // eslint-disable-next-line no-unused-expressions
    partKey
      ? Key[`${partKey}`] = payload[`${partKey}`] : null;
    // eslint-disable-next-line no-unused-expressions
    sortKey !== null
      ? Key[`${sortKey}`] = payload[`${sortKey}`] : null;
  }

  return {
    Key,
    UpdateExpression,
    ExpressionAttributeValues,
    ExpressionAttributeNames
  }
}

module.exports = {
  objToDynamo
}
