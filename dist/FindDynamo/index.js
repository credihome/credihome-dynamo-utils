/**
 * Filtra uma query por uma coluna em multiplos valores escolhidos.
 * @param {any[]} searchValues - Lista de valores para buscar
 * @param {string} columnName - Nome da coluna para filtrar
 */
const findDynamo = (searchValues, columnName) => {
  var searchObject = {};
  var index = 0;
  searchValues.forEach(function (value) {
    index++;
    var name = ':' + columnName + index;
    searchObject[name.toString()] = value;
  });

  return {
    ExpressionAttributeValues: searchObject,
    FilterExpression: (columnName + ' IN (' + Object.keys(searchObject).toString() + ')')
  }
}
module.exports = {
  findDynamo
}
